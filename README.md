```
pip install -r requirements.txt

export PYBINDPLUGIN=`/usr/bin/env python -c 'import pyangbind; import os; print ("{}/plugin".format(os.path.dirname(pyangbind.__file__)))'`

pyang --plugindir $PYBINDPLUGIN -f pybind -o binding.py opendaylight-inventory.yang
```

Refer to https://github.com/robshakir/pyangbind